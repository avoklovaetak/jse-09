package ru.volkova.tm.api;

public interface ICommandController {

    void showIncorrectCommand();

    void showIncorrectArgument();

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();

    void showSystemInfo();

    void exit();

}
